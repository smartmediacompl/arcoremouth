﻿Shader "Shaders/ArShaderLips" 
{
    Properties 
    {
        _Shininess ("Shininess", Range (0.03, 1)) = 0.078125
        _MainTex ("Base (RGB) Gloss (A)", 2D) = "white" {}
        _TintColor("Tint Color", Color) = (1,1,1,1)
        _Transparency("Transparency", Range(0.0,1)) = 0.25
        [NoScaleOffset] _BumpMap ("Normalmap", 2D) = "bump" {}
        _Amount ("Extrusion Amount", Range(-1,1)) = 0.05
    }
    SubShader 
    {
        // Tags { "RenderType"="Opaque" }
        Tags {"Queue"="Transparent" "RenderType"="Transparent" }
        LOD 100

        CGPROGRAM
        #pragma surface surf MobileBlinnPhong exclude_path:prepass nolightmap noforwardadd halfasview interpolateview finalcolor:lightEstimation vertex:vert

        
        sampler2D _MainTex;
        sampler2D _BumpMap;
        half _Shininess;
        fixed3 _GlobalColorCorrection;
        float _Transparency;
        float4 _TintColor;

        struct Input
        {
            float2 uv_MainTex;
        };
        struct v2f
        {
            float2 uv : TEXCOORD0;
            float4 vertex : SV_POSITION;
        };

        inline fixed4 LightingMobileBlinnPhong (SurfaceOutput s, fixed3 lightDir, fixed3 halfDir, fixed atten)
        {
            fixed diff = max (0, dot ( s.Normal , lightDir));
            fixed nh = max (0, dot ( s.Normal , halfDir));
            fixed spec = pow (nh, s.Specular*128) * s.Gloss;

            fixed4 c;
            

            // fixed3 ve = fixed3(0.6,0.6,0.6);
            // c.rgb = (s.Albedo *  (1- _LightColor0.rgb) * diff * 2);
            // c.rgb = (1/s.Albedo) * (_LightColor0.rgb) + .25f;
            // c.rgb = (s.Albedo *  _LightColor0.rgb * diff)-.55f;
            // c = c* 0.25 * _LightColor0.rgb;
            c.rgb = (s.Albedo * _LightColor0.rgb * diff + _LightColor0.rgb * spec) * atten;

            // c.rgb = s.Albedo *.25f;

            // c += _TintColor;
            UNITY_OPAQUE_ALPHA(c.a);
            return c;
        }

        void lightEstimation(Input IN, SurfaceOutput o, inout fixed4 color)
        {
            color.rgb *= _GlobalColorCorrection;
        }

        void surf (Input IN, inout SurfaceOutput o)
        {
            fixed4 tex = tex2D(_MainTex , IN.uv_MainTex);
            o.Albedo = tex.rgb + _TintColor.rgb ;
            o.Gloss = tex.a ;
            o.Alpha =  tex.a ;
            o.Specular = _Shininess;
            o.Normal = UnpackNormal (tex2D(_BumpMap, IN.uv_MainTex));
        }
        float _Amount;
        void vert (inout appdata_full v) {
            v.vertex.xyz += v.normal * _Amount;
        }

        fixed4 frag (Input i) : SV_Target
        {
            // sample the texture
            fixed4 col = tex2D(_MainTex, i.uv_MainTex) + _TintColor;
            col.a = _Transparency;
            // clip(col.r - _CutoutThresh);
            return col;
        }

        ENDCG
    }

    FallBack "Mobile/VertexLit"
}
