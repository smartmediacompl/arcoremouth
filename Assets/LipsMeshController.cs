﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LipsMeshController : MonoBehaviour
{
    public void GetMeshVertices(Vector3[] vertices)
    {
        Mesh mesh = GetComponent<MeshFilter>().mesh;
        mesh.vertices = vertices;
        mesh.RecalculateNormals();
    }
}
