﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorPick : MonoBehaviour
{
    public Renderer renderer;
    public Renderer renderer2;
    public ColorPicker picker;

    // Use this for initialization
    void Start()
    {
        picker.onValueChanged.AddListener(color =>
        {
            renderer.material.color = color;
            renderer2.material.color = color;
        });

        renderer.material.color = picker.CurrentColor;
        renderer2.material.color = picker.CurrentColor;

    }

    // Update is called once per frame
    void Update()
    {

	}
}
