﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataMouthLocalization
{
    public static readonly List<int> MOUTH_UPPER = new List<int>
    {
        //Upper lip (upper)
        12,11,302,
        12,302,268,
        268,302,303,
        268,303,271,
        271,303,304,
        271,304,272,
        272,304,408,
        272,408,407,
        407,408,306,
        407,306,292,
        38,72,11,
        38,11,12,
        41,73,72,
        41,72,38,
        42,74,73,
        42,73,41,
        183,184,74,
        183,74,42,
        78,62,183,
        78,183,191,


        //Upper lip (lower)
        13,12,268,
        13,268,312,
        312,268,271,
        312,271,311,
        311,271,272,
        311,272,310,
        310,272,407,
        310,407,415,
        415,407,292,
        415,292,308,
        82,38,12,
        82,12,13,
        81,41,38,
        81,38,82,
        80,42,41,
        80,41,81,
        191,183,42,
        191,42,80,
        62,76,184,
        62,184,183,


    };
    public static readonly List<int> MOUTH_BOTTOM = new List<int>
    {
         //Bottom lip (upper)
        16,15,316,
        16,316,315,
        315,316,403,
        315,403,404,
        404,403,319,
        404,319,320,
        320,319,325,
        320,325,307,
        307,325,308,
        307,308,292,
        85,86,15,
        85,15,16,
        180,179,86,
        180,86,85,
        90,89,179,
        90,179,180,
        77,96,89,
        77,89,90,
        78,96,77,
        78,77,62,



        //Bottom lip (lower)
        17,16,315,
        17,315,314,
        314,315,404,
        314,404,405,
        405,404,320,
        405,320,321,
        321,320,307,
        321,307,375,
        375,307,292,
        375,292,306,
        84,85,16,
        84,16,17,
        181,180,85,
        181,85,84,
        91,90,180,
        91,180,181,
        146,77,90,
        146,90,91,
        62,77,146,
        62,146,76
    };

    public static readonly int[] UPPER_LIP_TOP_LINE = {
        11,72,302,73,303,74,304,184,408
    };
    public static readonly int[] UPPER_LIP_MIDDLE_LINE = {
        12,38,268,41,271,42,272,183,407
    };
    public static readonly int[] UPPER_LIP_DOWN_LINE = {
        13,82,312,81,311,80,310,191,415
    };
    public static readonly int[] LOWER_LIP_TOP_LINE = {
        15,86,316,179,403,89,319,96,325
    };
    public static readonly int[] LOWER_LIP_MIDDLE_LINE = {
        16,85,315,180,404,90,320,77,307
    };
    public static readonly int[] LOWER_LIP_DOWN_LINE = {
        17,84,314,181,405,91,321,146,375
    };
    public static readonly int[] MOUTH_CORNERS = {
        76,62,78,308,292,306
    };
    public static readonly List<int> LIP_UPPER_MIDDLE = new List<int>{
        12,72,11,302
    };
    public static readonly List<int> LIP_UPPER_MIDDLE_LEFT_FIRST = new List<int>{
        38,73,72
    };
    public static readonly List<int> LIP_UPPER_MIDDLE_RIGHT_FIRST = new List<int>{
        268,302,303
    };
    public static readonly List<int> LIP_UPPER_MIDDLE_LEFT_SECOND = new List<int>{
        41,73,74
    };
    public static readonly List<int> LIP_UPPER_MIDDLE_RIGHT_SECOND = new List<int>{
        271,303,304
    };
    public static readonly List<int> UPPER_LIP_LINE = new List<int> {
        12,38,268,41,271,42,272,183,407
    };
    public static readonly List<int> LOWER_LIP_LINE = new List<int> {
        15,86,316,179,403,89,319,96,325
    };
}
