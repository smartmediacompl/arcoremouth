﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class MouthControllerMoveVectors : MonoBehaviour
{
    public Slider sliderLowerLipTopLine;
    public Slider sliderLowerLipMiddleLine;
    public Slider sliderLowerLipDownLine;
    List<int> _lips;
    public Slider sliderMeshControl;
    private void Start() {

        _lips = DataMouthLocalization.MOUTH_UPPER.Concat(DataMouthLocalization.MOUTH_BOTTOM).ToList();
    }
    public List<Vector3> ChangeMeshPosition(List<Vector3> m_MeshVertices)
    {
        HashSet<int> mouthHashSet = new HashSet<int>(_lips);
        if (sliderMeshControl != null) m_MeshVertices = SetPosition(m_MeshVertices, mouthHashSet.ToList(), sliderMeshControl.value);
        return m_MeshVertices;
    }
    public List<int> GetMeshIndicies(){
        return _lips;
    }
    //Mesh resize
    public List<Vector3> UpdateMouthPosistionAndSize(List<Vector3> m_MeshVertices)
    {
        m_MeshVertices = MovePointsAwayFromPoint(m_MeshVertices , DataMouthLocalization.LIP_UPPER_MIDDLE, sliderLowerLipDownLine.value);   

        m_MeshVertices = MovePointsAwayFromPoint(m_MeshVertices , DataMouthLocalization.LIP_UPPER_MIDDLE_LEFT_FIRST, sliderLowerLipMiddleLine.value);       
        m_MeshVertices = MovePointsAwayFromPoint(m_MeshVertices , DataMouthLocalization.LIP_UPPER_MIDDLE_RIGHT_FIRST, sliderLowerLipMiddleLine.value);       
        
        m_MeshVertices = MovePointsAwayFromPoint(m_MeshVertices , DataMouthLocalization.LIP_UPPER_MIDDLE_LEFT_SECOND, sliderLowerLipTopLine.value);       
        m_MeshVertices = MovePointsAwayFromPoint(m_MeshVertices , DataMouthLocalization.LIP_UPPER_MIDDLE_RIGHT_SECOND, sliderLowerLipTopLine.value);       

        return m_MeshVertices;
    }
    private List<Vector3> SetPosition(List<Vector3> m_MeshVertices, List<int> list, float changeValue)
    {
        for (int i = 0; i < list.Count; i++)
        {
            m_MeshVertices[list[i]] = new Vector3(
                m_MeshVertices[list[i]].x,
                m_MeshVertices[list[i]].y + changeValue,
                m_MeshVertices[list[i]].z);
        }

        return m_MeshVertices;
    }
    private List<Vector3> MovePointsAwayFromPoint(List<Vector3> m_MeshVertices , List<int> data, float distance)
    {
        Vector3 basePoint = m_MeshVertices[data[0]];
        for (int i = 1; i < data.Count; i++)
        {

            float fi = Mathf.Atan2(
                m_MeshVertices[data[i]].y - basePoint.y,
                m_MeshVertices[data[i]].x - basePoint.x
            );

            m_MeshVertices[data[i]] = new Vector3(
                m_MeshVertices[data[i]].x + distance * Mathf.Cos(fi),
                m_MeshVertices[data[i]].y + distance * Mathf.Sin(fi),
                m_MeshVertices[data[i]].z
            );
        }
        return m_MeshVertices;
    }

}