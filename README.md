# Augmented Face Example

![Augmented Face Example](https://github.com/jbyu/arcore_face/blob/master/example.jpg)
[video](https://www.youtube.com/watch?v=81g1xKj0Tbs)

Unity 2019.3.0f1

arcore-unity-sdk-1.14
https://github.com/google-ar/arcore-unity-sdk/releases/tag/v1.14.0

Unity wireframe renderer 
https://github.com/miguel12345/UnityWireframeRenderer
